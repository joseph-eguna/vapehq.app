import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { Registration } from "../models/registration.model";

@Injectable({
  providedIn: "root"
})

export class DataService {

	private _notification = new BehaviorSubject({
			loading:false,
			message: 'processing please wait!'
		})
	
	private _menus = new BehaviorSubject([
		{ title: 'Products', route:"/admin/product", icon: 'fa-home', active:true },
		{ title: 'Users', route:"/admin/user", icon: 'fa-user', active:false }
	]);

	private _currentUser = new BehaviorSubject({});

	private _isAdmin = new BehaviorSubject(false);


	notification = this._notification.asObservable();
	menus = this._menus.asObservable();
	currentUser = this._currentUser.asObservable();
	isAdmin = this._isAdmin.asObservable();

	notificationChange( loading, message='processing please wait!' ) { this._notification.next({ loading, message }); }

	menusChange( menus ) {this._menus.next(menus); }

	generateCurrentUser( user ) { this._currentUser.next( user ); }

	isAdminCheck( isAdmin:boolean ) { this._isAdmin.next(isAdmin); }
	
	/*
		private messageSource = new BehaviorSubject('default message');
		currentMessage = this.messageSource.asObservable();
		constructor() { }
		changeMessage(message: string) {
			this.messageSource.next(message)
		}
	*/

}