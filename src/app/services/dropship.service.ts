import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

import { environment as env } from 'src/environments/environment';

import { Product } from "src/app/models/product.model";
import { ProductEntry } from "../models/productentry.model";
import { Registration } from "../models/registration.model";
import { Log } from "../models/log.model";

let headers = new HttpHeaders({ 
	'authorization': window.localStorage['signin'],
	'uid': window.localStorage['uid'] });

let params  = new HttpParams();

@Injectable({ providedIn: "root" })
export class DropshipService {

	constructor(private http: HttpClient) { }

	uninstallApp( entry_id:any, data:any ) : Observable<any> {
		return this.http.post( `${env.APIURL}/vapehq-dropship/uninstall-app/${entry_id}`, data, { headers });
	} 

	websiteSaved( data:any ) : Observable<any> {
		return this.http.post( `${env.APIURL}/vapehq-dropship/website-saved-changes/`, data, { headers });
	}

	getSiteById( entry_id:any ) : Observable<any> {
		return this.http.get( `${env.APIURL}/vapehq-dropship/query-sites/${entry_id}`, { headers });
	} 

	migrateProducts( data:any) : Observable<any>{
		return this.http.post( `${env.APIURL}/vapehq-dropship/site-import-products`, data, { headers } );
	}

	trageckoWebhooksGET() : Observable<any> {
		return this.http.post<any>(`${env.APIURL}/vapehq-dropship/dropship-tradegecko-webhooks/`, { action: "list" }, { headers });
	}

	trageckoWebhooksGENERATE() : Observable<any> {
		return this.http.post<any>(`${env.APIURL}/vapehq-dropship/dropship-tradegecko-webhooks/`, { action: "generate" }, { headers });
	}

	trageckoWebhooksDESTROY() : Observable<any> {
		return this.http.post<any>(`${env.APIURL}/vapehq-dropship/dropship-tradegecko-webhooks/`, { action: "destroy" }, { headers });
	}

}



