import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

import { environment as env } from 'src/environments/environment';

import { Product } from "src/app/models/product.model";
import { ProductEntry } from "../models/productentry.model";
import { Registration } from "../models/registration.model";
import { Log } from "../models/log.model";

let headers = new HttpHeaders({ 
	'authorization': window.localStorage['signin'],
	'uid': window.localStorage['uid'] });

let params  = new HttpParams();

//const apiUrl = 'https://api.vapehq.co/vA3pQ';
//const apiUrl = 'http://localhost:8123';

@Injectable({ providedIn: "root" })
export class ProductService {

	constructor(private http: HttpClient) { }

	vapehqDropshipImportProducts( data:any ) : Observable<any> {
		return this.http.post( `${env.APIURL}/vapehq-dropship/site-import-products`, data, { headers });
	} 

	vapehqDropshipSiteApproval( data: any ) :Observable<any> {
		return this.http.post( `${env.APIURL}/vapehq-dropship/woocommerce-site-approval`, data, { headers });
	}

	vapehqGetTradegeckoWebhooks( token:any ) : Observable<any> {
		return this.http.get<any>(`${env.APIURL}/vapehq-dropship/tradegecko-webhooks/${token}`, { headers });
	}

	vapehqGenerateTradegeckoWebhooks( data:any ) : Observable<any> {
		return this.http.post<any>(`${env.APIURL}/vapehq-dropship/tradegecko-webhooks/`, data, { headers } );
	}

	vapehqDestroyTradegeckoWebhooks( token, site_url ) : Observable<any> {
		return this.http.delete<any>(`${env.APIURL}/vapehq-dropship/tradegecko-webhooks/${token}/${site_url}`, { headers });
	}

	/*
	 * Product
	 */
	productLists(): Observable<Product[]> {
	    return this.http.get<Product[]>(`${env.APIURL}/product`, { headers });
	}

	getProduct( productId ) : Observable<any> {
		return this.http.get<any>(`${env.APIURL}/product/${productId}`, { headers });
	}

	createProduct( data:any ) : Observable<Product> {
		return this.http.post<Product>(`${env.APIURL}/product/`, data, { headers });
	}

	updateProduct( data:any, productId ) : Observable<Product> {
		return this.http.put<Product>(`${env.APIURL}/product/${productId}`, data, { headers });
	}

	deleteProduct( productId ) : Observable<Product> {
		return this.http.delete<Product>(`${env.APIURL}/product/${productId}`, { headers });
	}

	/*
	 * Product Entry 
	 */
	productEntryLists( productId ): Observable<ProductEntry[]> {
	    return this.http.get<ProductEntry[]>(`${env.APIURL}/product/${productId}/entry`, { headers });
	}

	getEntryDetail( productId, entryId ) : Observable<ProductEntry> {
		return this.http.get<ProductEntry>(`${env.APIURL}/product/${productId}/entry/${entryId}`, { headers });
	} 

	createEntryDetail( data:any, productId ) : Observable<any> {
		return this.http.post( `${env.APIURL}/product/${productId}/entry`, data, { headers } );
	}

	updateEntryDetail( data:any, productId, entryId ) : Observable<any> {
		return this.http.put(`${env.APIURL}/product/${productId}/entry/${entryId}`, data, { headers });
	}

	deleteEntryDetail( productId, entryId ) : Observable<any> {
		return this.http.delete( `${env.APIURL}/product/${productId}/entry/${entryId}`, { headers } );
	}


	/*
	 * User
	 */
	usersList(): Observable<any> {
	    return this.http.get<any>(`${env.APIURL}/user/`, { headers });
	}

	getUser( userId ) : Observable<any> {
		return this.http.get<any>(`${env.APIURL}/user/${userId}`, { headers });
	} 

	createUser( data:any ) : Observable<Registration> {
		return this.http.post( `${env.APIURL}/user`, data, { headers } );
	}

	updateUser( data:any, userId) : Observable<Registration> {
		return this.http.put(`${env.APIURL}/user/${userId}`, data, { headers });
	}

	removeUser( userId ) : Observable<any> {
		return this.http.delete( `${env.APIURL}/user/${userId}`, { headers } );
	}
	

	/*
	 * Logs 
	 */
	getLogs( entryId, data:any ) : Observable<any> {
		return this.http.post<any>(`${env.APIURL}/log/list/${entryId}`, data, { headers });
	}

	testGet() : Observable<any> {
		/*params = params.set('a', 'a value').set('b', 'b value').set('c', 'c value');*/
		params = new HttpParams({fromString : `a=av&b=bv&c=cv`});
		return this.http.get(`${env.APIURL}/log/test`, { headers, params });
	}
	
	testPost() : Observable<any> {
		return this.http.post(`${env.APIURL}/log/test`, {}, { headers });
	}

	/*getLog( entryId, logId ) : Observable<ProductEntry[]> {
		return this.http.get<Log[]>(``);
	}

	updateLog( entryId, logId ) : Observable<ProductEntry[]> {
		return this.http.put<Log[]>(``);
	}

	deleteLog( entryId, logId ) : Observable<ProductEntry[]> {
		return this.http.delete(``);
	}*/

	/** get error logs **
	getErrorLogs() : Observable<ProductEntry[]> {
		return this.http.get<Log[]>(``);
	}
	
	getErrorLog( entryId, logId ) : Observable<ProductEntry[]> {
		return this.http.get<Log[]>(``);
	}

	updateErrorLog( entryId, logId ) : Observable<ProductEntry[]> {
		return this.http.put<Log[]>(``);
	}

	deleteErrorLog( entryId, logId ) : Observable<ProductEntry[]> {
		return this.http.delete(``);
	}*/

}



