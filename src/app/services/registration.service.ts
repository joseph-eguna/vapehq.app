import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, ReplaySubject } from "rxjs";

import { environment as env } from 'src/environments/environment';

//const apiUrl = 'https://api.vapehq.co/vA3pQ';
//const apiUrl = 'http://localhost:8123';

@Injectable({ providedIn: "root" })
export class RegistrationService {


	constructor(private http: HttpClient) { 
	
	}

	userRegistration( data:any ) : Observable<any> {
		return this.http.post( `${env.APIURL}/user/register`, data);
	} 

	userLogin( data:any ) : Observable<any> {
		return this.http.post( `${env.APIURL}/user/login`, data);
	} 

	userLogout( data:any ) : Observable<any> {
		return this.http.post( `${env.APIURL}/user/logout`, data);
	}

	userTokenValidation() : Observable<any> {
		return this.http.post( `${env.APIURL}/user/validate-token`, { token: window.localStorage['signin'] });
	} 

}
