import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { isAuthenticated } from './resolver/index';
import { AppRoutingModule } from './app-routing.module';

import { SharedModule } from './shared/shared.module';
import { AdminModule } from './admin/admin.module';
import { ViewModule } from './view/view.module';
import { AppComponent } from './app.component';

@NgModule({
declarations: [ AppComponent ],
imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    AdminModule,
    ViewModule
],
providers: [
    isAuthenticated
  ],
bootstrap: [AppComponent]
})
export class AppModule { }
