import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

//import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { RegistrationService } from 'src/app/services/registration.service';
import { Registration } from 'src/app/models/registration.model';
import { Error } from 'src/app/models/error.model';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

	details : Registration = {};
	errors : Error = {};

	login = false;
	submitted = false;
	message = '';

	constructor( private registration: RegistrationService,
				 private router :  Router,
				 private route : ActivatedRoute ) { }

	ngOnInit(): void {

		let { action } = this.route.snapshot.params;

		this.login = (action =='login') ? true : false;

		//console.log( window.localStorage['signin'] );

		//window.localStorage.removeItem('jwtToken');

	}

	registerUser() : void {		

		this.errors = {}; this.submitted = true;

		let { email, password, confirmpassword } = this.details;

		if( !this.isValidEmail( email ) ) { this.errors.email = `Invalid Email!`; return }

		if( !this.isPasswordMatch( password, confirmpassword ) ) { this.errors.password = `Password donot match!`; return }

		
		this.registration.userRegistration( this.details ).subscribe((response)=>{
				
				let { error, name, message } = response;
				
					console.log( { error, name, message, response } );

				if( error ) { this.errors[name] = message; return; }
				
				this.message = message;

				setTimeout( ()=>{ this.router.navigateByUrl(`/register/login`); }, 1000 );
		
			}, (error)=>{ console.log(`error in user registration`); });

	}

	loginUser() : void {

		this.errors = {}; this.submitted = true;

		this.registration.userLogin( this.details ).subscribe((response)=>{
				
			let { userDetails, Token, error, message } = response;

			if(error) { this.errors[`email`] = message; return; }

			if(!Token) { this.errors[`email`] = `Invalid Login`; return; }
			
			window.localStorage['signin'] = Token;

			window.localStorage['uid'] = userDetails._id;

			this.message = `Please wait while redirecting to admin page!`; 

			setTimeout(()=>{ 
				window.location.href = '/admin';
			}, 1000);

			console.log( response );

		}, (error)=>{ console.log(`user login error`); })

	}

	private isValidEmail( email ) : boolean {

		if(!email) return false;

		const check = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	
    	return check.test(String(email).toLowerCase());

	}

	private isPasswordMatch( password, passwordconfirm) :boolean { 

		if(!password || !passwordconfirm) return false;

		let isMatch = password.localeCompare(passwordconfirm);  

		return (isMatch === 0) ? true : false;

	}


}
