import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ViewRoutingModule } from './view-routing.module';
import { ViewComponent } from './view.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';


@NgModule({
  declarations: [ViewComponent, AuthenticationComponent, PagenotfoundComponent],
  imports: [
    CommonModule,
    FormsModule,
    ViewRoutingModule
  ]
})
export class ViewModule { }
