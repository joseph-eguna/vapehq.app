import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewComponent } from './view.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

const routes: Routes = [
	{ path: '', component: ViewComponent },
	{ path: 'register', component: AuthenticationComponent },
	{ path: 'register/:action', component: AuthenticationComponent },
	{ path: 'page-not-found', component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewRoutingModule { }
