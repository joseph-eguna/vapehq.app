export class Error {
	name?: string;
	email?: string;
	password?: string;
	confirmpassword?: string;
	message?: string;
	code?:string;
}
