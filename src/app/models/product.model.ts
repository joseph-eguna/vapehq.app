export class Product {

	id?: any;
	name?: any;
	type?:any;
	tradegecko_token?:any;
	hook_stockupdate?:any;
	hook_createupdate_order?:any;
	description?: any;
	order_prefix?: any;
	order_company_id?: any;
	order_number?: any;
	billing_address_id?: any;
	shipping_address_id?: any;
	woo_callback_url?: any;
	sho_appid?: any;
	sho_callback_url?: any;
	
}
