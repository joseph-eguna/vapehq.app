export class ProductEntry {
	_id?: any;
	name?: any;
	productid?: any;
	cms?:any;
	key?:any;
	secret?:any;
	key_id?:any;
	hook_ids?:any;
	authorize?: any;
	owner_id?:any;
	product_publish?:boolean;
	order_push?:boolean;

}
