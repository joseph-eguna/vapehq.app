export class Log {
	id?: any;
	app?: string;
	url?: string;
	type?: string;
	status?: string;
	sku?:string;
	product_id?: string;
	tradegecko_id?: string;
	message?: string;
}