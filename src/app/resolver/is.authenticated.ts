import { Injectable } from '@angular/core';
//import { HttpClient } from "@angular/common/http";
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';


import { RegistrationService } from 'src/app/services/registration.service';

@Injectable()

export class isAuthenticated implements Resolve<boolean> {
    constructor(
        private router: Router,
        private userService: RegistrationService
    ) {}

    resolve() : Promise<boolean> {

        const promise : Promise<boolean> = new Promise(( resolve, reject ) => {
            this.userService.userTokenValidation().subscribe((response)=>{
                let { result, error, decoded } = response; console.log(response);
                if(!result) { 
                    this.router.navigate(['/register/login']); reject(true); };
                resolve(true);

           }, (error)=>{ this.router.navigate(['/register/login']); reject(true); console.log(`error token`); });

        });

        return promise;

    }

}

