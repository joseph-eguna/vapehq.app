import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';


import { RegistrationService } from 'src/app/services/registration.service';
import { DataService } from 'src/app/services/data.service';

@Injectable()

export class isAdmin implements Resolve<boolean> {

    user = {};

    constructor(
        private router: Router,
        private userService: RegistrationService,
        private data : DataService
    ) {}

    resolve() : Promise<boolean> {

        const promise : Promise<boolean> = new Promise(( resolve, reject ) => {
            this.data.isAdmin.subscribe((boolean)=>{
                if(!boolean) {
                    this.router.navigate(['/admin']);
                }
                resolve(true);
            },(error)=>{});
            
        });

        return promise;

    }

}

