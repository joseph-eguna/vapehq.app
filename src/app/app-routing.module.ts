import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { isAuthenticated } from './resolver/index';

const routes: Routes = [
	{ path: '', loadChildren: () => import('./view/view.module').then(m => m.ViewModule) },
	{ path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule), resolve : { isAuthenticated } },
	{ path: '**', redirectTo: '/page-not-found' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }