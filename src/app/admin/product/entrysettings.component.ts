import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { environment as env } from 'src/environments/environment';

import { DataService } from 'src/app/services/data.service';
import { ProductService } from 'src/app/services/product.service';
import { DropshipService } from 'src/app/services/dropship.service';
import { ProductEntry } from 'src/app/models/productentry.model';

@Component({
	selector: 'product-entry-settings',
	templateUrl: './entrysettings.component.html',
	styleUrls: ['./product.component.css']
})
export class EntrySettingsComponent implements OnInit {

	@Input('settings') settings = {
		woo_callback_url:null
	};

	product_id : null;

	entry_id : null;

	entry_detail: ProductEntry = {};

	constructor( private route:ActivatedRoute, 
				 private productService:ProductService,
				 private dropshipService:DropshipService,
				 private dataService:DataService ) { }

	ngOnInit(): void { 

		let { id:product_id, eid:entry_id } = this.route.snapshot.params;

		this.product_id = product_id;
		
		this.entry_id = entry_id;
		
		this.getSiteDetail(entry_id);

	}

	getSiteDetail( entry_id ) : void {
		
		this.dropshipService.getSiteById( entry_id ).subscribe((r) => {console.log(r); this.entry_detail = r }, (e) => console.log(e))
	
	}

	saveChanges() : void {

		this.dataService.notificationChange(true, `Saved Changes!`);
		let saved = this.dropshipService.websiteSaved( this.entry_detail ).subscribe( (r) =>{

			console.log(r);
			this.dataService.notificationChange(false); 
		}, (e)=> console.log(e));

	}
	/*
	 * FOR WORDPRESS 
	 */
	importVapeHQProducts() : void {
		
		this.dataService.notificationChange(true, `generating migration at the backend`);

		this.dropshipService.migrateProducts( this.entry_detail ).subscribe((r) => {

			console.log(r); return this.dataService.notificationChange(false);

		},(e)=>console.log(e))
		
	}

	installWoocommerce() : void {

		let { _id:entry_id, name:url } = this.entry_detail;
		
		let params = {
			app_name:"VapeHQ Dropshipping",
			scope:"read_write",
			user_id:1,
			return_url: window.location.href,
			callback_url: `${env.APIURL}/vapehq-dropship/website-authentication/${this.entry_id}`
		}

		let redirect_url = url+"/wc-auth/v1/login/?";

		for (let key in params) redirect_url+=key+"="+encodeURIComponent(params[key])+"&";
		
		window.location.href = redirect_url;

	}

	uninstall() : void {

		this.dataService.notificationChange(true);

		this.dropshipService.uninstallApp( this.entry_id, this.entry_detail )
		.subscribe((r) => { 
			let {message} = r;  console.log( r );  this.getSiteDetail( this.entry_id )
			this.dataService.notificationChange(false, message);
		},e=>console.log(`uninstall error`));

	}

	/*
	 * FOR SHOPIFY 
	 */
	
	installShopify() : void {

		let { _id:entry_id, name:url } = this.entry_detail;
		
		let params = {
			client_id: env.DROPSHIP_SHOPIFY_KEY,
			scope: env.DROPSHIP_SHOPIFY_SCOPE,
			redirect_uri:`${env.APIURL}/vapehq-dropship/website-authentication/shopify/`
		}

		let redirect_url = url+"/admin/oauth/authorize?";
		for (let key in params)  redirect_url+=key+"="+encodeURIComponent(params[key])+"&";
		
		console.log(redirect_url); window.location.href = redirect_url;
	
	}

	uninstallShopify() : void { let {name} = this.entry_detail; window.location.href = `${name}/admin/apps`; }

	/*
	 * COMMON
	 */
	selectCms( cms ) : void {

		let { secret, cms:c } =  this.entry_detail;

		this.entry_detail.cms = cms; 
		
		console.log(!this.entry_detail.secret, (this.entry_detail.cms));

	}


}
