import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ProductService } from 'src/app/services/product.service';
import { ProductEntry } from '../../models/productentry.model';

@Component({
	selector: 'product-entry-errors',
	templateUrl: './entryerrors.component.html',
	styleUrls: ['./product.component.css']
})
export class EntryErrorsComponent implements OnInit {

	constructor( private route:ActivatedRoute, private productService:ProductService ) { }

	ngOnInit(): void { 


	}

}
