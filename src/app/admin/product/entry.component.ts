import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ProductService } from '../../services/product.service';
import { ProductEntry } from '../../models/productentry.model';

@Component({
	selector: 'product-entry',
	templateUrl: './entry.component.html',
	styleUrls: ['./product.component.css']
})
export class EntryComponent implements OnInit {
	
	_notification : {};

	settings = {};

	product_id : String;

	entry_id : String;

	entry_route: String = 'settings'; 

	constructor( private route:ActivatedRoute, private productService:ProductService ) { }


	ngOnInit(): void { 
		let { id, eid } = this.route.snapshot.params;
			this.product_id = id;
			this.entry_id = eid;

		let { route } = this.route.snapshot.data;
			this.entry_route = (!route) ? this.entry_route : route;
		this.getSettings();
	}


	getSettings() : void {
		this.productService.getProduct(this.product_id).subscribe((response)=>{
			let { woo_callback_url } = response; //console.log( response );
			this.settings = { woo_callback_url };
		},(error)=>{ return; },);
	}


}
