import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ProductService } from 'src/app/services/product.service';

@Component({
	selector: 'product-entry-logs',
	templateUrl: './entrylogs.component.html',
	styleUrls: ['./product.component.css']
})
export class EntryLogsComponent implements OnInit {

	Logs : {};
	Pagination : {};

 	constructor( private route:ActivatedRoute, private productService:ProductService ) { }

	ngOnInit(): void { 

		this.getLogs();

	}

	getLogs( paged=1, limit=5 ) : void {

		console.log({ paged });

		let { id, eid: entry_id } = this.route.snapshot.params;
		
		let data = { paged, limit }

		this.productService.getLogs(entry_id, data )
			.subscribe((response) => { 
				let {logs, count, error} = response; console.log(logs);
				if(error.length < 0 || !logs) return;
				this.Logs = logs;
				
				let max = Math.ceil(count/limit); 
				console.log(max);
				let previous = ((paged-1) > 0) ? paged-1 : null;
				let next = ((paged+1) <= max) ? paged+1 : null;
				
				this.Pagination = {
					previous,
					next
				}
			}, (error) => { console.log(error) });
		
	}

	paginate( paged ) : void { this.getLogs( paged ); }

	ngOnDestroy() : void { /*console.log( 'destroy' )*/	}

}