import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
	selector: 'product-detail',
	template: '<p>Product Detail</p>'
})
export class ProductDetailComponent implements OnInit {
	
	constructor( private route:ActivatedRoute ) { }


	ngOnInit(): void { 

		const id = this.route.snapshot.paramMap.get('id');

		console.log(id);

	}

}
