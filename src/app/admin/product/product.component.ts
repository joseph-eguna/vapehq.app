import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';

import { Product } from '../../models/product.model';
import { ProductEntry } from '../../models/productentry.model';
import { ProductService } from '../../services/product.service';
import { DataService } from '../../services/data.service';

@Component({
	selector: 'products',
	templateUrl: './product.component.html',
	styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

	menus = [];

	products : Product[];

	productEntries : ProductEntry[];

	newProductEntry : String = '';

	constructor( 
		private productService: ProductService, 
		private data: DataService,
		private router: Router,
		private route: ActivatedRoute
		 ) { }


	ngOnInit(): void { 

		this.productLists();

		this.getMenus();
	
	}

	productLists() : void {

		this.productService.productLists()
			.subscribe((response) => { 
				this.products = response; 
				this.products.map((v,k) => {
					this.productEntryLists(v.id); 
				});
			}, (error) => { console.log(`error in listing products`) });
	
	}

	productEntryLists( productId ) : void {

		this.productService.productEntryLists(productId)
			.subscribe( (response)=>{
				if(response) {
					response = response.filter((v, i)=>{
						if(v.owner_id == window.localStorage['uid']) 
							return v;
					})
					console.log(response);
					this.productEntries = response;
				}
			},(error)=>{ console.log(`error in listing product entry`) }, );
	
	}

	productEntryAdd( productId ) : void {

		//console.log( productId );
		//this.router.navigateByUrl(`/admin/product/${productId}/e/6013b116ecd13850dc6cb452`);

		let isValidUrl = this._isValidUrl( this.newProductEntry );

		if( !isValidUrl ) { console.log(`invalid site url`); return; };

		let data = { 
			name:this.newProductEntry,
			productid : productId,
			owner_id: window.localStorage['uid'] 
		}
		this.productService.createEntryDetail( data,  productId )
			.subscribe( (response)=>{
				let { _id:entry_id } = response;
				//console.log(response);
				this.productLists();
				this.router.navigateByUrl(`/admin/product/${productId}/e/${entry_id}`);
			}, ( error )=>{ console.log(`error in adding new entry`) } );

	}

	productEntryDelete( productId, entryId ) : void {

		let _confirm = confirm("Are you sure you wanted to delete this site?"); if(!_confirm) return;
		
		this.data.notificationChange( true, `deleting site entry` );
		
		this.productService.deleteEntryDetail( productId, entryId )
			.subscribe( (response)=>{
				//console.log(response);
				this.data.notificationChange( false );
				this.productLists();
			}, ( error )=>{ console.log(`error in deleting entry`) } );

	}

	getMenus() : void {

		this.data.menus.subscribe((menus)=>{
			if(!menus) return;
			this.menus = menus.map((v, i)=>{
				v.active = false;  if(v.title == 'Products') v.active = true; return v;
			});

		}, (error)=>{ return });

	}

	private _isValidUrl( url ) : boolean {

		let pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
		    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
		    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
		    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
		    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
		    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
		
		return pattern.test(url);
		
	}

}
