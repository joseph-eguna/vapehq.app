import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Product } from 'src/app/models/product.model';
import { DataService } from 'src/app/services/data.service';
import { ProductService } from 'src/app/services/product.service';
import { DropshipService } from 'src/app/services/dropship.service';


@Component({
	selector: 'product-detail',
	templateUrl: './productdetail.component.html',
	styleUrls: ['./product.component.css']
})
export class ProductDetailComponent implements OnInit {
	
	_notification : {};

	product_id : String;
	detail : Product = {};
	tradegeckoWebhooks = [];
	submitted = false;

	constructor( private route:ActivatedRoute, 
				 private productService:ProductService,
				 private dataService:DataService,
				 private dropshipService: DropshipService ) { }


	ngOnInit(): void { 

		let { id } = this.route.snapshot.params;
			this.product_id = id;

		this.getProductDetail();

	}


	save() : void {

		this.submitted = true;

		this.dataService.notificationChange(true);

		this.productService.updateProduct( this.detail, this.product_id ).subscribe((response)=>{

			this.dataService.notificationChange(false); console.log(response);

		}, (error)=>{ console.log(`error subscription`); });

	}

	getProductDetail() : void {

		this.productService.getProduct(this.product_id).subscribe((r)=>{

			this.detail =  r; return this.getTradegeckoWebhooks();

		},(e)=>{ console.log(`subscription error`); });
	
	}

	getTradegeckoWebhooks() : void {

		this.dropshipService.trageckoWebhooksGET().subscribe( (r)=>{

			console.log(r); this.dataService.notificationChange(false); return this.tradegeckoWebhooks = r;

		}, (error)=>{ console.log(`tradegecko webhooks error`); });
	
	}

	generateTradegeckoWebhooks() : void {
		
		this.dataService.notificationChange(true);

		this.dropshipService.trageckoWebhooksGENERATE().subscribe( (r)=>{

			console.log(r); this.dataService.notificationChange(false); return this.tradegeckoWebhooks = r;

		}, (error)=>{ console.log(`tradegecko webhooks error`); });

	}

	deleteTradegeckoWebhooks() : void {

		this.dataService.notificationChange(true);

		this.dropshipService.trageckoWebhooksDESTROY().subscribe( (r)=>{

			console.log(r); this.dataService.notificationChange(false); return this.tradegeckoWebhooks = r;

		}, (error)=>{ console.log(`tradegecko webhooks error`); });
	
	}


}
