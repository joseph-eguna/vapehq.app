import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { isAdmin, isAuthenticated } from '../resolver/index';

import { 
	AdminComponent, 
	ProductComponent, 
	EntryComponent,
	ProductDetailComponent,
	UserComponent,
	UserDetailComponent } from './index';

const routes: Routes = [
	{ path: '', component: AdminComponent },
	{ path: 'product', component: ProductComponent, resolve: { isAuthenticated } },
	{ path: 'product/:id/edit', component: ProductDetailComponent, resolve : { isAdmin } },
	{ path: 'product/:id/e/:eid', component: EntryComponent },
	{ path: 'product/:id/e/:eid/logs', component: EntryComponent, data: { route: "logs" } },
	{ path: 'product/:id/e/:eid/errors', component: EntryComponent, data: { route: "errors" } },
	{ path: 'user', component: UserComponent, resolve : { isAdmin } },
	{ path: 'user/:id', component: UserDetailComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AdminRoutingModule { }
