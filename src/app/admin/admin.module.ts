import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { isAdmin } from '../resolver/index';

import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../shared/shared.module' //
import { HeaderComponent, SidebarComponent } from './layout/index'; //

import { 
	AdminComponent, 
	ProductComponent, 
	ProductDetailComponent,
	EntryComponent,
	EntryLogsComponent,
	EntrySettingsComponent,
	EntryErrorsComponent,
	UserComponent,
	UserDetailComponent } from './index';

@NgModule({
  declarations: [
	AdminComponent, 
	HeaderComponent, 
	SidebarComponent, 
	ProductComponent,
	ProductDetailComponent, 
	EntryComponent,
	EntrySettingsComponent,
	EntryLogsComponent,
	EntryErrorsComponent,
	UserComponent,
	UserDetailComponent ],
  imports: [
    CommonModule,
    FormsModule,
    AdminRoutingModule,
    SharedModule
  ],
  providers : [ isAdmin ],
  exports:[],
})
export class AdminModule { }
