import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ProductService } from 'src/app/services/product.service';

import { Registration } from 'src/app/models/registration.model';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

	menus = [];

	users : Registration;


	constructor( private data : DataService,
				 private user : ProductService ) { }

	ngOnInit(): void {
		
		this.getMenus();

		this.getUsers();
	}

	getMenus() : void {

		this.data.menus.subscribe((menus)=>{
			if(!menus) return;
			this.menus = menus.map((v, i)=>{
				v.active = false;  if(v.title == 'Users') v.active = true; return v;
			});

			//console.log(this.menus);
		}, (error)=>{ return });

	}

	getUsers() : void {
		
		this.user.usersList().subscribe((users)=>{
			if(!users) return;
			this.users = users;
			this.data.notificationChange(false);
			//console.log(this.users);
		}, (error)=>{});
	
	}

	editUser( userId ) : void {
		
	}

	approveUser( userId ) : void {
		
		this.data.notificationChange(true)
		this.user.updateUser( { status: 'approve' }, userId ).subscribe((user)=>{
			if( !user ) return;
			this.getUsers();
		},(error)=>{});

	}

	declineUser( userId ) : void {
		
		this.data.notificationChange(true)
		this.user.updateUser( { status: 'decline' }, userId ).subscribe((user)=>{
			if( !user ) return;
			this.getUsers();
		},(error)=>{});

	}

	removeUser( userId ) : void {
		
		let _confirm = confirm("Are you sure you wanted to delete this user?"); if(!_confirm) return;
		this.data.notificationChange(true);
		this.user.removeUser( userId ).subscribe((user)=>{
			if( !user ) return;
			this.getUsers();
		},(error)=>{});

	}

}
