import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { DataService } from 'src/app/services/data.service';
import { Registration } from 'src/app/models/registration.model';

@Component({
	selector: 'sidebar-component',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./layout.component.css']
})
export class SidebarComponent implements OnInit {

	@Input() menus = '';

	currentUser : Registration = {};

	constructor( private router: Router,
				 private productService : ProductService,
				 private data : DataService ) { }

	ngOnInit(): void {
		this.getCurrentUser(); //console.log( this.menus, 'sidebar' );
	}

	userLogout() : void {
		window.localStorage.removeItem('signin');
		window.location.href="/register/login";
	}

	getCurrentUser() : void {

		this.data.currentUser.subscribe((user)=>{ 
			if(!user || Object.keys(user).length < 1)
				return this.generateCurrentUser();
			this.currentUser = user;
			//console.log(user);
		});

	}

	private generateCurrentUser() : void {

		let id = window.localStorage['uid'];
		this.productService.getUser( id ).subscribe((user) => {
			if( !user ) return;
			let { email, roles } = user
				user['text'] = email.substring(0, 3).toUpperCase();
			
			this.currentUser = user;
			this.data.generateCurrentUser( user ); //console.log(user);

			let isAdmin = (roles.indexOf('administrator') != -1) ? true : false;
			this.data.isAdminCheck(isAdmin);

		}, (error) => { console.log(`error getting current user`); }, );

	}

}
