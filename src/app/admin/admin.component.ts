import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

//import { DataService } from 'src/app/services/data.service';
//import { take } from 'rxjs/operators';

@Component({
	selector: 'app-admin',
	templateUrl: './admin.component.html',
	styleUrls: ['./admin.component.css']
})

export class AdminComponent implements OnInit {

	constructor(
        private router: Router
    ) {}

	ngOnInit(): void {
		this.router.navigate(['/admin/product']);
	}

	logout(): void {
		//this.data.authenticationChange(false);
	}
}
