import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { ProductEntry } from 'src/app/models/productentry.model';
import { ProductService } from 'src/app/services/product.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	products : Product[];

	productEntries : ProductEntry[];

	productEntryId : string;


	constructor( private productService: ProductService ) { }


	ngOnInit(): void { this.productLists(); }


	addProductEntry() : void {};

	editProductEntry() : void {

		
	} 

	removeProductEntry() : void{}


	productLists() : void {

		this.productService.productLists()
			.subscribe((response) => { 
				this.products = response; 
				this.products.map((v,k) => {
					this.productEntryLists(v.id); 
				});
			}, (error) => { console.log(error) });
	}

	productEntryLists( productId ) : void {

		this.productService.productEntryLists(productId)
			.subscribe( (response)=>{
				this.productEntries = response;
				console.log(response);
			},(error)=>{

			}, );
	
	}

}
