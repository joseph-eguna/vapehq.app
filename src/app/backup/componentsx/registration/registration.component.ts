import { Component, OnInit } from "@angular/core";
import { Registration } from "src/app/models/registration.model";
import { RegistrationService } from "src/app/services/registration.service"

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

	submitted : boolean;

	registration : Registration;

	message : string;

	constructor( private registrationService: RegistrationService ) { }

	ngOnInit(): void {};

	signup() : void {

		const data = {
			email : this.registration.email,
			password : this.registration.password
		}

		this.registrationService.create( data )
			.subscribe((response) => { this.message = response.message; console.log(response) }, (error) => { console.log(error) });
	}

}

