import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
	selector: 'app-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

	notification : Object = {};

	constructor( private data: DataService ) { }

	ngOnInit(): void {

		this.data.notification
			.subscribe((response)=>{ this.notification = response; 
					}, (error)=>{ console.log(`notification error`) });

	}

}
