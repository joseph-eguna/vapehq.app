import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineComponent } from './timeline/timeline.component';
import { NotificationComponent } from './notification/notification.component';

@NgModule({
	declarations: [
		TimelineComponent, 
		NotificationComponent
	],
	imports: [
		CommonModule 
	],
	exports : [
		TimelineComponent, 
		NotificationComponent
	]
})
export class SharedModule { }
