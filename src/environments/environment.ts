// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	APIURL: 'http://localhost:8123', //'https://api.vapehq.co/vA3pQ';
	DROPSHIP_SHOPIFY_KEY: '9c5204efff8aaf7bff287ec4331762fe',
	DROPSHIP_SHOPIFY_SECRET: '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',
	DROPSHIP_SHOPIFY_SCOPE: 'read_products,write_products,read_inventory,write_inventory,read_orders,write_orders,read_fulfillments,write_fulfillments'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
